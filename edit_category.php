<?php
session_start();
if (empty ($_SESSION['login'])) {
    header('Location: http://' . $_SERVER['SERVER_NAME'] . '/index.php');
    exit;
}
#error_reporting(E_ALL);
#ini_set('display_errors', 1);
include __DIR__ . "/Classes/Category.php";
$current = new Category($_SESSION['login']);

if (isset($_GET['addDebit'])) {
    $current->addCategory($_SESSION['login'], $_GET['newDebit'], 'debit');
}

if (isset($_GET['deleteDebit'])) {
    $current->deleteCategory($_SESSION['login'], $_GET['choiseDebit'], 'debit');
}

if (isset($_GET['addDeposit'])) {
    $current->addCategory($_SESSION['login'], $_GET['newDeposit'], 'deposit');
}

if (isset($_GET['deleteDeposit'])) {
    $current->deleteCategory($_SESSION['login'], $_GET['choiseDeposit'], 'deposit');
}
include __DIR__ . "/edit_category.html";
?>

