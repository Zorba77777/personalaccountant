<?php
session_start();
if (empty ($_SESSION['login'])) {
    header('Location: http://' . $_SERVER['SERVER_NAME'] . '/index.php');
    exit;
}

#error_reporting(E_ALL);
#ini_set('display_errors', 1);
include __DIR__ . "/Classes/Transaction.php";
$trans = new Transaction();
$trans->loadTransactions($_SESSION['login']);
$count = count($trans->transactionsList);
include __DIR__ . "/showAccount.html";
?>
