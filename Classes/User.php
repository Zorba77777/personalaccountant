<?php
include __DIR__ . "/StringHelper.php";

class User
{
    private $mysqli;

    public function __construct()
    {
        $this->mysqli = new mysqli("localhost", "root", "123456", "finance");
    }

    public function checkLoginPassword($login, $password)
    {
        $login = StringHelper::clean($login);
        $password = StringHelper::clean($password);

        $res = $this->mysqli->query("SELECT login, password FROM users WHERE login='$login'");
        $row = $res->fetch_array();
        $result = false;
        if (($row['login'] == $login) && (password_verify($password, $row['password']))) {
            $result = true;
        }
        return $result;
    }

    public function checkLogin($login)
    {
        $login = StringHelper::clean($login);

        $res = $this->mysqli->query("SELECT login FROM users WHERE login='$login'");
        $row = $res->fetch_array();
        $result = false;
        if (!empty($row['login'])) {
            $result = true;
        }
        return $result;
    }

    public function saveUser($login, $password, $name, $surname, $city)
    {
        $login = StringHelper::clean($login);
        $password = StringHelper::clean($password);
        $password = password_hash($password, PASSWORD_DEFAULT);
        $name = StringHelper::clean($name);
        $surname = StringHelper::clean($surname);
        $city = StringHelper::clean($city);
        $this->mysqli->query("
        INSERT INTO users (login, password, name, surname, city) 
          VALUES('$login','$password', '$name', '$surname', '$city')
          ");
    }
}