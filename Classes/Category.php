<?php
session_start();

class Category
{
    public $array_of_deposit_category = [];
    public $array_of_debit_category = [];
    private $mysqli;

    public function __construct($login)
    {
        $this->mysqli = new mysqli("localhost", "root", "123456", "finance");
        $res = $this->mysqli->query("SELECT login FROM categories WHERE login='$login'");
        $row = $res->fetch_array();
        if (empty($row['login'])) {
            $this->mysqli->query("
        INSERT INTO categories (login, category, typeOfCategory) 
          VALUES('$login','payment', 'deposit'), ('$login','meat', 'debit'),
          ('$login','others', 'deposit'), ('$login','fish', 'debit') 
          ");
        }
        $res = $this->mysqli->query("
            SELECT category 
              FROM categories 
              WHERE login='$login' AND typeOfCategory='deposit'
          ");
            $this->array_of_deposit_category = $res->fetch_all();
            $res = $this->mysqli->query("
            SELECT category 
              FROM categories 
              WHERE login='$login' AND typeOfCategory='debit'
          ");
            $this->array_of_debit_category = $res->fetch_all();
        }

    public function addCategory($login, $newCategory, $typeOfCategory)
    {
        if ($typeOfCategory == 'debit') {
            $this->mysqli->query("
        INSERT INTO categories (login, category, typeOfCategory) 
          VALUES('$login','$newCategory', 'debit')
          ");
            $this->array_of_debit_category[][0] = $newCategory;

        } elseif ($typeOfCategory == 'deposit') {
            $this->mysqli->query("
        INSERT INTO categories (login, category, typeOfCategory) 
          VALUES('$login','$newCategory', 'deposit')
          ");
            $this->array_of_deposit_category[][0] = $newCategory;
        }
    }

    public function deleteCategory($login, $delCategory, $typeOfCategory)
    {
        $this->mysqli->query("
        DELETE FROM categories
          WHERE login = '$login' AND category = '$delCategory' 
          AND typeOfCategory = '$typeOfCategory'
          ");
        $res = $this->mysqli->query("
            SELECT category 
              FROM categories 
              WHERE login='$login' AND typeOfCategory='deposit'
          ");
        $this->array_of_deposit_category = $res->fetch_all();
        $res = $this->mysqli->query("
            SELECT category 
              FROM categories 
              WHERE login='$login' AND typeOfCategory='debit'
          ");
        $this->array_of_debit_category = $res->fetch_all();
    }
}