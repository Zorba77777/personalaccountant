<?php

class StringHelper
{
    public static function clean($string) {
        $string = stripslashes($string);
        $string = htmlspecialchars($string);
        $string = trim($string);
        return $string;
    }
}