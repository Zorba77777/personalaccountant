<?php

//error_reporting(E_ALL);
//ini_set('display_errors', 1);
session_start();
include __DIR__ . "/Classes/User.php";
$editUs = new User();

if ((isset($_POST['subm'])) && (empty($_POST['names']) || empty($_POST['surname']) ||
        empty($_POST['city']) || empty($_POST['login']) || empty($_POST['password']))
) {
    $lackOfInformation = "Вы ввели не всю информацию. Пожалуйста, заполните все поля!";
}

if ((isset($_POST['subm'])) && (!empty($_POST['names'])) && (!empty($_POST['surname'])) &&
    (!empty($_POST['city'])) && (!empty($_POST['login'])) && (!empty($_POST['password']))) {
    if ($editUs->checkLogin($_POST['login'])) {
        $loginAlreadyExisted = "Извините, введённый вами логин уже зарегистрирован. Введите другой логин.";
    } else {
        $editUs->saveUser($_POST['login'], $_POST['password'], $_POST['names'], $_POST['surname'], $_POST['city']);
        $_SESSION['login'] = $_POST['login'];
        echo 'Регистрация прошла успешно. Через 5 секунд вы будете перенаправлены на главную страницу сайта.';
        header('Refresh: 5; URL=http://' . $_SERVER['SERVER_NAME'] . '/main.php');
        exit;
    }
}
include __DIR__ . "/reg.html";
?>