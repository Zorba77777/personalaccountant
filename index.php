<?php
session_start();
include __DIR__ . "/Classes/User.php";
$editUs = new User();

if (!empty($_POST['login']) && !empty($_POST['password'])) {
    $result = $editUs->checkLoginPassword($_POST['login'], $_POST['password']);
}
if (empty($_SESSION['login'])) {
    $entranceEntry = "Вы вошли на сайт, как гость<br><a href='#'>Эта ссылка на главную страницу сайта доступна только  зарегистрированным пользователям</a><br><br><br>";
} else {
    $entranceEntry = "Вы вошли на сайт, как " . $_SESSION['login'] . "<br><a  href='main.php'>Эта ссылка на главную страницу сайта доступна только  зарегистрированным пользователям</a><br><br><br>";
}

if ((isset($_POST['submit'])) && ((empty($_POST['login'])) || (empty($_POST['password'])))) {
    $warning = "Вы ввели не всю информацию. Пожалуйста, заполните все поля!";
}
if ($result) {
    $_SESSION['login'] = $_POST['login'];
    header('Location: http://' . $_SERVER['SERVER_NAME'] . '//main.php');
    exit;
} elseif (isset($_POST['submit']) && !empty ($_POST['login']) && !empty ($_POST['password'])) {
    $warning = "Вы ввели неверный логин/пароль. Повторите ввод еще раз или зарегистрируйтесь.";
}
include __DIR__ . "/initial.html";
?>

