<?php
session_start();
if (empty ($_SESSION['login'])) {
    header('Location: http://' . $_SERVER['SERVER_NAME'] . '/index.php');
    exit;
}
#error_reporting(E_ALL);
#ini_set('display_errors', 1);
include __DIR__ . "/Classes/Account.php";
include __DIR__ . "/Classes/Transaction.php";
include __DIR__ . "/Classes/Category.php";
$current = new Account($_SESSION['login']);
$trans = new Transaction($_SESSION['login']);
$categ = new Category($_SESSION['login']);

if (isset ($_GET['result'])) {
    if ($_GET['type'] == "deposit") {
        $current->deposit($_GET['summ']);
        $current->saveAccount($_SESSION['login']);
        $trans->saveTransaction($_SESSION['login'], $_GET['summ'], $_GET['choiseDeposit'], $_GET['type']);
    } elseif ($_GET['type'] == "debit") {
        if (!$current->debit($_GET['summ'])) {
            echo "<p style=\"color:red; font-size: 20px;\">Вы не можете проводить операции на суммы, большие, чем у вас есть денег на счете</p>";
        } else {
            $current->saveAccount($_SESSION['login']);
            $trans->saveTransaction($_SESSION['login'], $_GET['summ'], $_GET['choiseDebit'], $_GET['type']);
        }
    }
}
if (isset ($_GET['logout'])) {
    unset ($_SESSION['login']);
    header('Location: http://' . $_SERVER['SERVER_NAME'] . '/index.php');
    exit;
}
include __DIR__ . "/main.html";
?>
